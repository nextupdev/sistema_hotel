class CreateCashings < ActiveRecord::Migration
  def change
    create_table :cashings do |t|
      t.datetime :start_at
      t.datetime :end_at
      t.decimal :phisic_value, :precision => 10, :scale => 2
      t.decimal :logic_value, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
