class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.string :cpf
      t.string :rg
      t.date :birthday
      t.string :phone
      t.string :city
      t.string :uf
      t.string :email

      t.timestamps
    end
  end
end
