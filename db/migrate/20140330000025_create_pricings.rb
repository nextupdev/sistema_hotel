class CreatePricings < ActiveRecord::Migration
  def change
    create_table :pricings do |t|
      t.integer :amount
      t.decimal :price, :precision => 10, :scale => 2
      t.references :classification, index: true

      t.timestamps
    end
  end
end
