class AddAtCashToBilling < ActiveRecord::Migration
  def change
    add_column :billings, :at_cash, :boolean
  end
end
