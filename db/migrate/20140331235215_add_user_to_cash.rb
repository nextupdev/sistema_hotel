class AddUserToCash < ActiveRecord::Migration
  def change
    add_reference :cashes, :user, index: true
  end
end
