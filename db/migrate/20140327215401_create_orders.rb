class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :booking, index: true
      t.references :product, index: true
      t.integer :amount

      t.timestamps
    end
  end
end
