class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.timestamp :start_at
      t.timestamp :end_at
      t.references :room, index: true

      t.timestamps
    end
  end
end
