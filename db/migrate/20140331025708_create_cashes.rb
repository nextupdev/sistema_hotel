class CreateCashes < ActiveRecord::Migration
  def change
    create_table :cashes do |t|
      t.datetime :start_at
      t.datetime :end_at
      t.decimal :value_open, :precision => 10, :scale => 2
      t.decimal :value_phisic_closed, :precision => 10, :scale => 2
      t.decimal :value_system_closed, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
