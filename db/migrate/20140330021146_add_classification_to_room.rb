class AddClassificationToRoom < ActiveRecord::Migration
  def change
    add_reference :rooms, :classification, index: true
  end
end
