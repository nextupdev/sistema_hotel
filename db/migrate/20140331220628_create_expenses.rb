class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string :description
      t.decimal :value, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
