class CreateBillings < ActiveRecord::Migration
  def change
    create_table :billings do |t|
      t.references :booking, index: true
      t.boolean :paid
      t.decimal :value, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
