class AddHostedToBooking < ActiveRecord::Migration
  def change
    add_column :bookings, :hosted, :boolean
  end
end
