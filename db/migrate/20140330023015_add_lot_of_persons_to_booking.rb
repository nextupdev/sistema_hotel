class AddLotOfPersonsToBooking < ActiveRecord::Migration
  def change
    add_column :bookings, :lot_of_persons, :integer
  end
end
