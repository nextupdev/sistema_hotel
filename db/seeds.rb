# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#Tipo de Quartos e precos dos quartos

User.create :username => "administrador", :password => 'senhamaster', :email => "admin@admin.com", :superadmin => true

classification = Classification.create :name => "Quarto Solteiro Simple" , :description => " 1 cama box, ar condicionado, TV, racker e frigobar"
		classification.pricings.create :amount => " 1 " , :price => " 50,00 "
		classification.rooms.create :number => " 05 " , :description => ""
		classification.rooms.create :number => " 06 " , :description => ""
		classification.rooms.create :number => " 10 " , :description => ""
		classification.rooms.create :number => " 11 " , :description => ""
		classification.rooms.create :number => " 12 " , :description => ""
		classification.rooms.create :number => " 17 " , :description => ""
		classification.rooms.create :number => " 18 " , :description => ""
		classification.rooms.create :number => " 19 " , :description => ""

classification = Classification.create :name => "Quarto Casal" , :description => " 1 cama de casal, ar condicionado, TV, racker e frigobar"
		classification.pricings.create :amount => " 1 " , :price => " 100,00 "

		classification.rooms.create :number => " 09 " , :description => ""
		classification.rooms.create :number => " 22 " , :description => ""
		classification.rooms.create :number => " 23" , :description => ""
		classification.rooms.create :number => " 24" , :description => ""




classification = Classification.create :name => "Quarto Solteiro Duplo" , :description => " 2 camas box, ar condicionado, TV, racker e frigobar"
		classification.pricings.create :amount => " 1 " , :price => " 50,00 "
		classification.pricings.create :amount => " 2 " , :price => " 90,00 "

		classification.rooms.create :number => " 01 " , :description => ""
		classification.rooms.create :number => " 02" , :description => ""
		classification.rooms.create :number => " 03 " , :description => ""
		classification.rooms.create :number => " 04 " , :description => ""
		classification.rooms.create :number => " 08 " , :description => ""
		classification.rooms.create :number => " 15 " , :description => ""
		classification.rooms.create :number => " 20 " , :description => ""
		classification.rooms.create :number => " 21" , :description => ""


classification = Classification.create :name => "Quarto Solteiro Triplo" , :description => " 3 camas box, ar condicionado, TV, racker e frigobar"
		classification.pricings.create :amount => " 1 " , :price => " 70,00 "
		classification.pricings.create :amount => " 2 " , :price => " 90,00 "
		classification.pricings.create :amount => " 2 " , :price => " 120,00 "
		classification.pricings.create :amount => " 2 " , :price => " 140,00 "

		classification.rooms.create :number => " 13 " , :description => ""


classification = Classification.create :name => "Quarto Familia" , :description => "1 cama box, 1 cama de casal, ar condicionado, TV, racker e frigobar"
		classification.pricings.create :amount => " 1 " , :price => " 70,00 "
		classification.pricings.create :amount => " 2 " , :price => " 100,00 "
		classification.pricings.create :amount => " 3 " , :price => " 135,00 "
		
		classification.rooms.create :number => " 07 " , :description => ""
		classification.rooms.create :number => " 14 " , :description => ""


classification = Classification.create :name => "Apartamento Master" , :description => "1 cama QUEE, 1 cama box(solteiro) , ar condicionado, TV, racker e frigobar"
		classification.pricings.create :amount => " 1 " , :price => " 100,00 "
		classification.pricings.create :amount => " 2 " , :price => " 150,00 "
		classification.pricings.create :amount => " 3 " , :price => " 180,00 "

		classification.rooms.create :number => " 16 " , :description => ""
		classification.rooms.create :number => " 25 " , :description => ""


#Numero dos Quartos



