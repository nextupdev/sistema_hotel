Sample::Application.routes.draw do

	devise_for :users
	get "home/index"
	root :to => "home#index"
	resources :bookings, :customers, :products, :billings, :cashes, :expenses, :users

	resources :rooms do
		resources :bookings, :module => :rooms
	end

	resources :bookings do
		get 'choose_classification', on: :new
		get 'billing', on: :member
		post 'billing' => "bookings#create_billing", :as => "billings"
		resources :orders, :module => :bookings
	end

	resources :classifications do
		resources :pricings, :module => :classifications
		collection { post :import }
	end

	resource :user, only: [:edit] do
		collection do
			patch 'update_password'
		end
	end
end
