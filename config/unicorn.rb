root = "/home/vagrant/sistemahotel"
working_directory root

# Unicorn PID file location
# pid "/path/to/pids/unicorn.pid"
pid "#{root}/tmp/pids/unicorn.pid"

# Path to logs
# stderr_path "/path/to/log/unicorn.log"
# stdout_path "/path/to/log/unicorn.log"
stderr_path "#{root}/log/unicorn.log"
stdout_path "#{root}/log/unicorn.log"

# Unicorn socket
listen "/tmp/unicorn.sistemahotel.sock"

# Number of processes
# worker_processes 4
worker_processes 2

# Time-out
timeout 30
