Time.class_eval do
   def after?(time)
      self > time if time.is_a?(Time)
   end

   def before?(time)
      self < time if time.is_a?(Time)
   end
   
   def is_past?
      self < Time.now.at_beginning_of_day
   end

   def at_midday
   	 self.at_beginning_of_day + 12.hours
   end
end
