# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Sample::Application.config.secret_key_base = 'ab73f8fc880fd8f3e452347a06ab09f82c417560953a71bc07de27720c1c33ce23009c650a8f71df7b0fee00fe42ed0c9aa9430e311992a805a780d9e1dfb7bf'
