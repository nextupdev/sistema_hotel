# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Sample::Application.initialize!

module Currency
  BRL = {:delimiter => ".", :separator => ",", :unit => "R$", :precision => 2, :position => "before"}
  USD = {:delimiter => ',', :separator => ".", :unit => "US$", :precision => 2, :position => "before"}
  DEFAULT = USD.merge(:unit => "$")

  module String
    def to_number(options={})
      return self.gsub(/,/, '.').to_f if self.numeric?
      nil
    end

    def numeric?
      self =~ /^(|-)?[0-9]+((\.|,)[0-9]+)?$/ ? true : false
    end
  end

end

class String; include Currency::String; end

