require 'spec_helper'

describe Booking do 
  before :each do 
    @room = create(:room)
    @customer = create(:customer)
  end

  it 'should not save without a room' do
    booking = Booking.new
    expect(booking).to_not be_valid
  end

  it 'is invalid when ends before starts' do
    @booking = build(:booking, room: @room, customer: @customer, start_at:Date.tomorrow, end_at: Date.today)
    expect(@booking).to_not be_valid
  end

  context 'to same room' do
    before :each do 
      create(:booking_from_today_until_next_week, room: @room, customer: @customer)
    end

    it 'should not create for reserved dates' do
      expect(build(:booking_from_today_until_tomorrow, room: @room, customer: @customer)).to_not be_valid     
      expect(build(:booking_from_today_until_next_week, room: @room, customer: @customer)).to_not be_valid
      expect(build(:booking_from_tomorrow_until_next_week, room: @room, customer: @customer)).to_not be_valid
      expect(build(:booking_between_today_and_next_week, room: @room, customer: @customer)).to_not be_valid
      expect(build(:booking_between_tomorrow_and_next_month, room: @room, customer: @customer)).to_not be_valid
    end 
  end


end 
