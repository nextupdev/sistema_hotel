require "spec_helper"

describe "Editing a customer", js: true do
	before(:each) do
		@customer = create(:customer)
		DatabaseCleaner.strategy = :transaction
		DatabaseCleaner.start
	end

	it "select a customer" do
		visit customers_path
		find("#customer-row-#{@customer.id}").click_link("Editar")
		fill_in "Nome", :with => "Hare hare"
		click_button "Cadastrar"
		expect(page).to have_content("Cliente atualizado com sucesso")
		expect(page).to have_content("Hare hare")
	end

	after(:each) do
		DatabaseCleaner.clean
	end
  
end