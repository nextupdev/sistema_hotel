require 'spec_helper'

describe "Add order to booking", js: true do
	before(:each) do
		@booking= create(:booking)
		@product = create(:product)
		DatabaseCleaner.strategy = :transaction
		DatabaseCleaner.start
	end

	it 'should be succesful' do
		visit booking_path(@booking)
		click_link "Registrar pedido"
		select @product.name, :from => "Product"
		fill_in "Amount", with: 10
		click_button "Salvar"
		expect(page).to have_content("Cadastrado com sucesso")
		expect(page).to have_content(@product.name)

	end

	after(:each) do
		DatabaseCleaner.clean
	end
  
end