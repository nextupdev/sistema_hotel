require 'spec_helper'

describe "Create new booking"  do
	before :each do
		@room = create(:room)
		@customer = create(:customer)
	end

	describe 'with valid fields filled', js: true do
		it "add in the calendar" do
			visit bookings_path
			click_link "Nova Reserva"
			within('.form') do
				fill_in "De", :with => Date.today
				fill_in "Até", :with => Date.tomorrow
				select @room.number, :from => 'Quarto'
				select @customer.name, :from => 'Cliente'
				click_button 'Cadastrar'
			end
			expect(page).to have_css('.alert-success')
			expect(page).to have_content("Reserva cadastrada com sucesso!")
			expect(page).to have_content(Booking.last.as_json[:title])
		end
	end

	describe 'with invalid dates', js: true do
		it 'not end at less than start at' do
			visit bookings_path
			click_link "Nova Reserva"
			within('.form')  do
				fill_in "De", :with => Date.tomorrow
				fill_in "Até", :with => Date.today
				click_button 'Cadastrar'
			end
			expect(page).to have_css('.alert-danger')
			expect(page).to have_content("Data de final não pode ser menor que data de início")
		end
	end

	describe 'with a room reserved', js: true do
		before(:each) do
			@booking = create(:booking, :room => @room, :customer => @customer)
		end

		it 'not be add new in same range' do
			visit bookings_path
			click_link "Nova Reserva"
			within('.form')  do
				fill_in "De", :with => Date.today
				fill_in "Até", :with => Date.tomorrow
				select @booking.room.number, :from => "Quarto"
				select @booking.customer.name, :from => "Cliente"
				click_button 'Cadastrar'
			end
			expect(page).to have_css('.alert-danger')
			# expect(page).to have_content("Já existe uma reserva entre as datas selecionadas")
		end
	end

	describe "cannot save between two dates", js: true do
		before(:each) do
			@booking = create(:booking, 
				:room => @room, 
				:customer => @customer, 
				:start_at => Date.today,
				:end_at => 4.day.since(Date.today)
			) 
		end

		it 'should not be successufl' do
			visit bookings_path
			click_link "Nova Reserva"
			within('.form')  do
				fill_in "De", :with => (Date.today+1.day)
				fill_in "Até", :with => (Date.today+2.days)
				select @booking.room.number, :from => "Quarto"
				select @booking.customer.name, :from => "Cliente"
				click_button 'Cadastrar'
			end
			expect(page).to have_css('.alert-danger')
			# expect(page).to have_content("Já existe uma reserva entre as datas selecionadas")
		end
	end



end