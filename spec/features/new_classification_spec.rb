require 'spec_helper'

describe 'create a new classification', js: true do
  it 'shoul be successful' do
      visit classifications_path
      click_link "Cadastrar tipo de quarto"
      @classification = build(:classification)
        fill_in "Nome", with: @classification.name
        fill_in "Descrição", with: @classification.description
        click_button "Salvar"
      expect(page).to have_content("Cadastrado com sucesso")
      expect(page).to have_content(@classification.name)
  end
end
