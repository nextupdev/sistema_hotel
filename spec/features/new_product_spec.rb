require 'spec_helper'

describe "Creante new product", js: true do
	it "should be succesful" do
		visit products_path
		click_link "Novo Produto"
		fill_in "Nome", :with =>  "Prodtuo #{001}"
		fill_in "Preço", :with =>  "10"
		click_button "Salvar"
		expect(page).to have_content("Cadastrado com sucesso")
	end
  
end