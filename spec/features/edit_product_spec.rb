require 'spec_helper'

describe "Editing a product", js: true do
	before(:each) do
		@product = create(:product)
		DatabaseCleaner.strategy = :transaction
		DatabaseCleaner.start
	end

	it "click link" do
		visit products_path
		find("#product-row-#{@product.id}").click_link "Editar"
		fill_in "Nome", with: "Coca-cola"
		click_button "Salvar"
		expect(page).to have_content("Editado com sucesso")
		expect(page).to have_content("Coca-cola")
	end

	after(:each) do
		DatabaseCleaner.clean
	end
end