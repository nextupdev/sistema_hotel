FactoryGirl.define do
  factory :booking do
  	 start_at Date.today
	 end_at Date.tomorrow
     room
     customer
     factory :booking_from_today_until_tomorrow do
	     start_at Date.today
	     end_at Date.tomorrow
     end
     
     factory :booking_from_today_until_next_week do
	     start_at Date.today
	     end_at 1.week.since(Date.today)
     end

     
     factory :booking_from_tomorrow_until_next_week do
	     start_at Date.tomorrow
	     end_at 1.week.since(Date.today)
	 end

	 factory :booking_between_today_and_next_week do
	     start_at Date.today
	     end_at 1.week.since(Date.today)
     end

     factory :booking_between_tomorrow_and_next_month do
	     start_at Date.tomorrow
	     end_at 2.week.since(Date.today)
     end
  end


end