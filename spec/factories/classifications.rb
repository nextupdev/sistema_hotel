FactoryGirl.define do
  factory :classification do
     name "Quarto simples 1"
     description "Quarto com cama de solteiro, ventilador e TV"
  end
end
