// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks

//= require jquery.ui.draggable
//= require jquery.ui.droppable
//= require jquery.ui.resizable
//= require jquery.ui.selectable

//= require bootstrap
//= require jquery.rest
//= require fullcalendar
//= require bootbox
//= require_tree .
//= require meiomask
//= require_self
jQuery.fn.serializeObject = function() {
  var arrayData, objectData;
  arrayData = this.serializeArray();
  objectData = {};

  $.each(arrayData, function() {
    var value;

    if (this.value != null) {
      value = this.value;
    } else {
      value = '';
    }

    if (objectData[this.name] != null) {
      if (!objectData[this.name].push) {
        objectData[this.name] = [objectData[this.name]];
      }

      objectData[this.name].push(value);
    } else {
      objectData[this.name] = value;
    }
  });

  return objectData;
};

$(document).ready(function() {
  // $("input[type=text].currency").setMask('decimal');
  bootbox.setDefaults({locale: "br"});

  $.rails.allowAction = function(element) {
    var message = element.data('confirm'),
    answer = false, callback;
    if (!message) { return true; }

    var callback = function(){
      complete = $.rails.fire(element,
        'confirm:complete', [answer]);
      if(complete) {
        var oldAllowAction = $.rails.allowAction;
        $.rails.allowAction = function() { return true; };
        element.trigger('click');
        $.rails.allowAction = oldAllowAction;
      }
    }

    if ($.rails.fire(element, 'confirm')) {
      bootbox.confirm(message, function(result) {
        if (result){
          callback();
        }
      });
      return false;
    }
    return false;
  }
});
