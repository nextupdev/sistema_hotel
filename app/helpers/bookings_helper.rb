module BookingsHelper
	def total_to_booking(booking)
		number_to_currency(booking.orders.total+booking.price)
	end
end
