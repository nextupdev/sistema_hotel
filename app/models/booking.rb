class Booking < ActiveRecord::Base
  belongs_to :room
  belongs_to :customer
  has_many :orders, dependent: :destroy
  has_one :billing
  scope :reserved_at_room, lambda{ |room| where("room_id=?", room.id) }
  scope :excepts, lambda {|booking| where.not(:id=>booking.id)  }
  scope :starts_between, lambda {|start_date, end_date| where(:start_at=>(start_date..end_date))}
  scope :ends_between, lambda {|start_date, end_date| where(:end_at=>(start_date..end_date))}
  scope :between_dates, lambda{ |start_date = Date.today,end_date=Date.tomorrow| where(["(start_at BETWEEN :start_at AND :end_at OR end_at BETWEEN :start_at AND :end_at)", {:start_at=> start_date, :end_at=> end_date}]) }
  scope :at_dates, lambda{ |start_date = Date.today,end_date=Date.tomorrow| where("(? BETWEEN start_at AND end_at OR ? BETWEEN start_at AND end_at)", start_date,end_date) }
  scope :today, lambda{ where("(? BETWEEN start_at AND end_at)", Date.today).first }

  validates_presence_of :start_at, :end_at, :room_id, :customer_id

  validate :cannot_start_or_end_at_reserved_date, :cannot_end_at_be_less_than_start_at, :if => lambda{ |object| object.start_at.present? and object.end_at.present? }
  validate :cannot_start_at_past, :if => lambda{ |object| object.start_at.present? }
  validate :cannot_end_at_past, :if => lambda{ |object| object.end_at.present?}

  before_validation(on: [:create,:save,:update]) do
      self.start_at = start_at.at_midday+1.second
      self.end_at = (end_at.today?) ? end_at.at_midday+1.day : end_at.at_midday
  end

  def paid?
    (!self.billing.nil?)
  end

  def cannot_start_or_end_at_reserved_date
    errors.add(:base, :invalid) if self.room.bookings.excepts(self).between_dates(start_at, end_at).exists? || room.bookings.excepts(self).at_dates(start_at, end_at).exists?
  end

  def cannot_end_at_be_less_than_start_at
    errors.add(:end_at, :inclusion) if self.end_at < self.start_at
  end
  
  def cannot_start_at_past
    errors.add(:start_at, :is_past) if self.start_at.is_past?
  end

  def cannot_end_at_past
    errors.add(:end_at, :is_past) if self.end_at.is_past?
  end

  
  scope :between, lambda {|start_time, end_time|
    {:conditions => ["? < starts_at < ?", start_time, end_time] }
  }

  def as_json(options = {})
    {
      :id => self.id,
      :_id => "#{self.id}",
      :title => "Reserva ##{self.id}",
      :start => self.start_at,
      :end => self.end_at,
      :start_at => self.start_at,
      :end_at => self.start_at,
      :allDay => true
    }
  end

  def self.format_date(date_time)
    Time.at(date_time.to_i).to_formatted_s(:db)
  end

  def price
    self.room.classification.pricings.find_by(:amount=>self.lot_of_persons).price*self.total_de_dias
  end

  def final_price
    (self.price.to_f + self.orders.total.to_f )
  end

  def price_total
    (self.price + self.orders.to_a.sum { |p| p.product.price * p.amount } )
  end

  scope :total, lambda { all.to_a.sum { |booking| booking.price} }
  scope :total_in_datetime_range, lambda { |start_at, end_at| all.to_a.sum { |booking| booking.price} }

  def total_de_dias
    ((self.end_at.day-self.start_at.day).days/1.day).to_i
  end
end
