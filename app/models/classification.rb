class Classification < ActiveRecord::Base
	has_many :pricings
	has_many :rooms
	def self.to_csv(options = {})
		options[:col_sep] = ";"
		CSV.generate(options) do |csv|
			csv << column_names
			all.each do |product|
				csv << product.attributes.values_at(*column_names)
			end
		end
	end

	def self.import(file)
		CSV.foreach(file.path, {headers: true, :col_sep=>";"}) do |row|
			classification = Classification.find_or_initialize_by(row.to_hash)
			begin
				classification.save 
			rescue ActiveRecord::StatementInvalid
				# registro já existe
			end
		end
	end
end
