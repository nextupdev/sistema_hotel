class Expense < ActiveRecord::Base
	scope :after_datetime, lambda { |datetime| where("created_at>?", datetime) }
	scope :total_after_datetime, lambda { |datetime| where("created_at>?", datetime).to_a.sum { |booking| booking.value} }
end
