class Ability
  include CanCan::Ability

  def initialize(user)
      if user.superadmin?
        can :manage, :all
      else
        can :manage, [Booking, Expense, Billing, Customer]
        can [:create, :update], Cash
        can :read, :rooms
      end
  end
end
