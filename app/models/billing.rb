class Billing < ActiveRecord::Base
  belongs_to :booking
  validates :booking_id, uniqueness: true, presence: true
  scope :after_datetime, lambda { |datetime| where("created_at>?", datetime) }
  scope :after_datetime_at_cash, lambda { |datetime| where("created_at>? and at_cash=?", datetime, true) }
  scope :total_after_datetime, lambda { |datetime| where("created_at>?", datetime).to_a.sum { |booking| booking.value} }
  scope :total_after_datetime_at_cash, lambda { |datetime| where("created_at>? and at_cash=?", datetime, true).to_a.sum { |booking| booking.value} }
end
