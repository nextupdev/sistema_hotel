class Room < ActiveRecord::Base
	#@self.per_page = 5
	has_many :bookings 
	belongs_to :classification
	validates :number, :uniqueness => true, :presence => true

	scope :reserved_at_date, lambda{ |date = Time.now.at_midday| joins(:bookings).where("? BETWEEN bookings.start_at AND bookings.end_at", date) }
	scope :busy_at_date, lambda{ |date = Time.now.at_midday| joins(:bookings).where("? BETWEEN bookings.start_at AND bookings.end_at", date) }
	scope :free_at_date, lambda{ |date = Time.now.at_midday| where("id NOT IN (SELECT room_id FROM bookings WHERE (? between start_at and end_at))", date) }

	def vacant?
		!bookings.exists?(["(? BETWEEN start_at AND end_at)", Time.now])
	end

	def reserved?
		return bookings.where("(? BETWEEN start_at AND end_at) and bookings.hosted=?", Date.today, false).exists?
	end

	def busy?
		return bookings.where("(? BETWEEN start_at AND end_at) and bookings.hosted=?", Date.today, true).exists?
	end

	def has_bookings_between_dates?(start_date, end_date)
		bookings.where(["(start_at BETWEEN :start_at AND :end_at OR end_at BETWEEN :start_at AND :end_at)", {:start_at=> start_date, :end_at=> end_date}]).exists?
	end 

	def has_bookings_at_dates?(start_at, end_at)
		return bookings.where("(? BETWEEN start_at AND end_at OR ? BETWEEN start_at AND end_at)", start_at,end_at).exists?
	end
end
