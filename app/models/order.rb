class Order < ActiveRecord::Base
  scope :total, 0
  belongs_to :booking
  belongs_to :product

  def total_price
  	(product.price*amount)
  end

  def self.total
  	sum { |o| o.product.price.to_i*o.amount }
  end
end
