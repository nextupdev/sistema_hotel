class Customer < ActiveRecord::Base
	self.per_page = 5
	validates_presence_of :birthday, :name
end
