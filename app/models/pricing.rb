class Pricing < ActiveRecord::Base
  belongs_to :classification

  def to_s
  	 self.amount + number_to_currency(self.price)
  end
end
