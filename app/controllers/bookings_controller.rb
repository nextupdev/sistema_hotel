class BookingsController < ApplicationController
	# load_and_authorize_resource param_method: :booking_params

	def index
		@bookings = Booking.all.joins(:room).includes(:room)
		respond_to do |format|
			format.html
			format.json { render :json => @bookings.to_json }
		end
	end

	def new
		@booking = Booking.new unless !@booking.nil?
		@classification = Classification.find(params[:classification_id])
		if (params[:booking])
			@booking = Booking.new(booking_params)
		end
		@booking.start_at=Time.now
		@booking.end_at=Time.now+1.day
	end

	def create
		@booking = Booking.new(booking_params)
		if @booking.save
			flash[:notice] = "Hospedagem cadastrada com sucesso!"
		end
	end

	def edit
		@booking = Booking.find(params[:id])
		@classification = @booking.room.classification
	end

	def update
		@booking = Booking.find(params[:id])
		if @booking.update_attributes(booking_params)
			flash[:notice] = "Reserva atualizada com sucesso!"
		end
	end

	def show
		@booking = Booking.find(params[:id])
		respond_to do |format|
			format.html
			format.js
			format.pdf do
				render :pdf => "relatorio", disposition: 'attachment', :layout => "application"
			end
		end
	end

	def destroy
		@booking = Booking.find(params[:id])
		@billing = @booking.billing
		if @booking.destroy
			flash[:notice] = "Removido com sucesso"
			@billing.cancelado = true
			@billing.save
		end
	end

	def choose_classification
		@booking = Booking.new(booking_params)
	end

	def billing
		@booking = Booking.find(params[:id])
		@billing = @booking.build_billing(:value=>@booking.price_total, :paid=>true, :at_cash=>true)
	end

	def create_billing
		@booking = Booking.find(params[:booking_id])
		@billing = @booking.create_billing(billing_params)
		@billing.paid = true
		@billing.save
		
	end

	def billing_params
		billing_params = params.require(:billing).permit(:value, :at_cash)
                billing_params[:value] = billing_params[:value].to_number
                billing_params
	end

	private
	def booking_params
		booking_params = params.require(:booking).permit(:start_at, :end_at, :room_id, :customer_id, :hosted, :lot_of_persons)
		booking_params
	end

end
