class Bookings::OrdersController < ApplicationController
	before_action :set_booking
	before_action :set_order, only: [:edit, :update, :show, :destroy]


	def new
		@order =  @booking.orders.build
	end

	def create
		@order = @booking.orders.build(order_params)
		if @order.save
			flash[:notice] = "Cadastrado com sucesso"
		end
	end

	def update
		if @order.update_attributes(order_params)
			flash[:notice] = "Atualizado com sucesso"
		end
	end

	private
	def set_order
		@order = @booking.orders.find(params[:id])
	end

	def set_booking
		@booking = Booking.find(params[:booking_id])
	end

	def order_params
		params.require(:order).permit(:amount, :product_id)
	end
end