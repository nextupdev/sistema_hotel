class RoomsController < ApplicationController
  before_action :set_room, only: [:destroy, :edit, :show, :update]
  load_and_authorize_resource param_method: :room_params

  def index
    @rooms = Room.paginate(:page => params[:page])
  end

  def new
    @room = Room.new
  end

  def create
    @room = Room.new(room_params)
    if @room.save
      @rooms = Room.paginate(:page => params[:page])
      flash[:notice] = "Cadastrado com sucesso"
    end
  end

  def edit
  end

  def show
  end

  def destroy
    if @room.destroy
      @rooms = Room.paginate(:page => params[:page])
      # redirect_to rooms_path, :notice => "Quarto removido"
    end
  end

  def set_room
    @room = Room.find(params[:id])
  end

  private
   def room_params
   	 params.require(:room).permit(:number, :description, :classification_id)
   end
end
