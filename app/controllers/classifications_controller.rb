class ClassificationsController < ApplicationController
	load_and_authorize_resource param_method: :classification_params
	before_filter :set_classification, only: [:edit, :update, :show, :destroy]

	def index
		@classifications = Classification.paginate(:page=>params[:page])
		respond_to do |format|
			format.html 
			format.csv { send_data Classification.all.to_csv }
		end
	end

	def import
		Classification.import(params[:file])
		redirect_to classifications_path
	end

	def new
		@classification = Classification.new
	end

	def create
		@classification = Classification.new(classification_params)
		if @classification.save
			flash[:notice] = "Cadastrado com sucesso"
		end
	end

	def edit
	end

	def update
		if @classification.update_attributes(classification_params)
			flash[:notice] = "Atualizado com sucesso"
		end
	end

	def show
	end

	def destroy
		if @classification.destroy
			flash[:notice] = "Removido com sucesso"
		end
	end

	private
	def set_classification
		@classification = Classification.find(params[:id])
	end

	def classification_params
		params.require(:classification).permit(:name, :description)
	end
end
