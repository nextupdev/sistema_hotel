class ExpensesController < ApplicationController
  before_action :set_expense, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource param_method: :expense_params

  # GET /expenses
  # GET /expenses.json
  def index
    begin
      cash = Cash.find(session[:cash_id])
      @expenses = Expense.where("created_at>?", cash.start_at)
      @total = Expense.total_after_datetime(cash.start_at)
    rescue ActiveRecord::RecordNotFound
      @expenses = Array.new
      @total = 0
    end
  end

  # GET /expenses/1
  # GET /expenses/1.json
  def show
  end

  # GET /expenses/new
  def new
    @expense = Expense.new
  end

  # GET /expenses/1/edit
  def edit
  end

  # POST /expenses
  # POST /expenses.json
  def create
    @expense = Expense.new(expense_params)

    if @expense.save
      flash[:notice] = "Cadastrado com sucesso"
      @total = Expense.total_after_datetime(current_user.current_sign_in_at)
    end
  end

  # PATCH/PUT /expenses/1
  # PATCH/PUT /expenses/1.json
  def update
      if @expense.update(expense_params)
        flash[:notice] = "Atualizado com sucesso"
      end
  end

  # DELETE /expenses/1
  # DELETE /expenses/1.json
  def destroy
    @expense.destroy
    respond_to do |format|
      format.html { redirect_to expenses_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_expense
      @expense = Expense.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def expense_params
      expense_params = params.require(:expense).permit(:description, :value)
      expense_params[:value] = expense_params[:value].to_number 
      expense_params
    end
  end
