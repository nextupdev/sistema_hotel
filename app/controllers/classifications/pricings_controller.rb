class Classifications::PricingsController < ApplicationController
	before_action :set_classification
	load_and_authorize_resource param_method: :pricing_params

	def index
		@pricings = @classification.pricings.paginate(:page=>params[:page])
	end

	def new
		@pricing = @classification.pricings.build
	end

	def create
		@pricing = @classification.pricings.build(pricing_params)
		if @pricing.save
			flash[:notice] = "Preço cadastrado com sucesso"
		end
	end

	def edit
		@pricing = @classification.pricings.find(params[:id])
	end

	def update
		@pricing = @classification.pricings.find(params[:id])
		@pricing.attributes = pricing_params
		if @pricing.save
			flash[:notice] = "Preço alterado com sucesso"
		end
	end

	def destroy
		@pricing = @classification.pricings.find(params[:id])
		if @pricing.destroy
			flash[:notice] = "Removido com sucesso"
		end
	end

	private
	def set_classification
		@classification = Classification.find(params[:classification_id])
	end

	def pricing_params
		pricing_params = params.require(:pricing).permit(:amount, :price)
		pricing_params[:price] = pricing_params[:price].to_number 
		pricing_params
	end
end
