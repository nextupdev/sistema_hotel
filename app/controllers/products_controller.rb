class ProductsController < ApplicationController
  before_action :set_product, only: [:edit,:update, :show, :destroy]
  load_and_authorize_resource param_method: :product_params
  
  def index
     @products = Product.paginate(:page => params[:page])
  end

  def new
    @product = Product.new
  end

  def create
     @product = Product.new(product_params)
     if @product.save
        flash[:notice] = "Cadastrado com sucesso"
     end
  end

  def edit
  end

  def update
    if @product.update_attributes(product_params)
        flash[:notice] = "Editado com sucesso"
    end
  end

  def destroy
    if @product.destroy
        flash[:notice] = "Produto removido com sucesso"
    end
    
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end
    
    def product_params
       product_params = params.require(:product).permit(:name, :price, :preco)
       product_params[:price] = product_params[:price].to_number 
       product_params
    end
end
