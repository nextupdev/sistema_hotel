class CashesController < ApplicationController
	load_and_authorize_resource param_method: :cash_params

	def index
		@cashes= Cash.all
		respond_to do |format| 
		   format.html
		   format.pdf { render :pdf => "relatorio_registro_de_caixas", disposition: 'attachment', :layout => "application" }
		end
	end

	def new
		@cash = Cash.new
	end

	def create
		@cash = Cash.new(cash_params)
		@cash.user = current_user
		@cash.start_at = Time.now
		if @cash.save
			session[:cash_id] = @cash.id
			redirect_to root_url
		end
	end

	def edit
		@cash = Cash.find(params[:id])
		total_billing = Billing.total_after_datetime_at_cash(@cash.start_at)
		total_expense = Expense.total_after_datetime(@cash.start_at)
		@cash.value_system_closed = @cash.value_open+total_billing-total_expense
	end

	def update
		@cash = Cash.find(params[:id])
		@cash.end_at = Time.now
		total_billing = Billing.total_after_datetime_at_cash(@cash.start_at)
		total_expense = Expense.total_after_datetime(@cash.start_at)
		@cash.value_system_closed = @cash.value_open+total_billing-total_expense
                @cash.value_phisic_closed = cash_params[:value_phisic_closed]
		@cash.user = current_user
		p @cash
		if @cash.save
			redirect_to root_url
		end
	end

	def cash_params
		cash_params = params.require(:cash).permit(:value_open, :value_phisic_closed, :value_system_closed)
                cash_params[:value_open] = cash_params[:value_open].to_number unless cash_params[:value_open].nil?
                cash_params[:value_phisic_closed] = cash_params[:value_phisic_closed].to_number unless cash_params[:value_phisic_closed].nil?
                cash_params
	end


end
