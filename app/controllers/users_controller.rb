class UsersController < ApplicationController
	load_and_authorize_resource param_method: :user_params


	def index
		@users = User.all
	end

	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		@user.email = @user.username+"@sistemahotel.com"
		if @user.save
			flash[:notice] = "Cadastrado com sucesso"
		else
			p @user.errors
		end
	end

	def show
		@user = current_user
	end

	def edit
		@user = current_user
	end

	def update_password
		@user = User.find(current_user.id)
		if @user.update_attributes(user_params)
	   	    sign_in @user, :bypass => true
      		flash[:notice] = "Senha atualizada."
		end
	end

	private
	def user_params
		params.require(:user).permit(:username, :password, :password_confirmation)
	end
end
