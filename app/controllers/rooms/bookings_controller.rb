class Rooms::BookingsController < ApplicationController
	before_action :set_room
	before_action :set_booking, only: [:show, :edit, :update, :destroy]
	def new
		@booking = @room.bookings.build(:start_at=>Date.today, :end_at=>Date.tomorrow)
	end

	def create
		@booking = @room.bookings.build(booking_params)
		@booking.hosted = true
		if @booking.save
			flash[:notice] = "Cadastrado com sucesso"
		else
			flash[:notice] = "Erro ao cadastrar hospedagem"
		end
	end

	def edit
	end

	private

	def set_room
		@room  = Room.find(params[:room_id])
	end

	def set_booking
		@booking = Booking.find(params[:id])
	end

	def booking_params
		params.require(:booking).permit(:start_at, :end_at, :customer_id, :lot_of_persons)
	end
end