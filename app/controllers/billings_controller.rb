class BillingsController < ApplicationController
	load_and_authorize_resource
	def index 
	   	@billings = Billing.where("created_at>?", current_user.current_sign_in_at)
		respond_to do |format| 
		   format.html
		   format.pdf { render :pdf => "relatorio_pagamentos_diario", disposition: 'attachment',  :layout => "application" }
		end
	end
end