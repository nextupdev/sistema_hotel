class CustomersController < ApplicationController
	load_and_authorize_resource param_method: :customer_params
	before_action :set_customer, only: [:show, :destroy, :edit, :update]

	def index
		@customers = Customer.paginate(:page => params[:page])
	end

	def new
		@customer = Customer.new
	end

	def create
		@customer = Customer.new(customer_params)
		if @customer.save
			flash[:notice] = "Cliente cadastrado com sucesso"
		end
	end

	def update
		@customer.attributes = customer_params
		if @customer.save
			flash[:notice] = "Cliente atualizado com sucesso"
		end
	end

	def destroy
		if @customer.destroy
			flash[:notice] = "Cliente excluído com sucesso"
		end
	end

	private
		def set_customer
			@customer = Customer.find(params[:id])
		end
		def customer_params
			params.require(:customer).permit(:name, :cpf, :rg, :birthday, :email, :phone)
		end
end
